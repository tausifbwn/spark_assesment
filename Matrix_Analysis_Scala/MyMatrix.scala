import scala.collection.mutable.ArrayBuffer

object MyObject {
def main(args: Array[String] ): Unit = {
  print("Enter the number of TestCases and Input for each testcase: \n")

  var testCases = scala.io.StdIn.readInt()
  var allArgs: List[Tuple4[Int,Int,Int,Array[Array[Int]]]] = List()
  for (i <- 1 to testCases) {
    var line1 = scala.io.StdIn.readLine()
    var lineArr = line1.split(" ")
    val n = lineArr(0).toInt
    val m = lineArr(1).toInt
    val b = lineArr(2).toInt

    var matrixList: List[List[Int]] = List()
    for (i <- 1 to n) {
      var line = scala.io.StdIn.readLine()
      var lineList = line.split(" ").toList.map(_.toInt)
      //println("lineList= " + lineList)
      matrixList = matrixList :+ lineList
      //println("matrixList= " + matrixList)
    }
    var matrixArray = matrixList.map(_.toArray).toArray
    var argVal = (n, m, b, matrixArray)
    allArgs = allArgs :+ argVal
  }

  /*The first line contains an integerT   , the number of test cases.
  • The first line of each test   case contains three integersN
  , MandB - the number of rows
  , the number of columns
  , and the intended happiness respectively
  • Each of the nextNlines of the test
  case containsMspace - separated integers
  Thej - th element in thei -th line represents the elementA[i][j] in thei - th row andj - th column */

  //var m = 3 //scala.io.StdIn.readInt()
  //var n = 3 //scala.io.StdIn.readInt()

  def getHappiness(n:Int,m:Int,b:Int,matrix:Array[Array[Int]]) : Int= {
    //println(matrix.toList.map(_.toList))
    val possibleRowCombination = Range(1, n + 1).toSet[Int].subsets.filter(_.nonEmpty).map(_.toList).toList
    val possibleColCombination = Range(1, m + 1).toSet[Int].subsets.filter(_.nonEmpty).map(_.toList).toList
    //println("possibleRowCombination= " + possibleRowCombination)
    //println("possibleColCombination= " + possibleColCombination)

    var allHappinessValue: List[Int] = List()
    for (rows <- possibleRowCombination) {
      for (cols <- possibleColCombination) {
        var subsetMatrix = rows.map(r => cols.map(c => (r, c))).flatMap(x => x)
        //println("subsetMatrix= " + subsetMatrix)
        var happinessValue = subsetMatrix.map(p => matrix(p._1 - 1)(p._2 - 1)).sum
        //println("happinessValue= " + happinessValue)
        allHappinessValue = allHappinessValue :+ happinessValue
      }
    } //End of Loop
    println("allHappinessValue= " + allHappinessValue )
    println("intended happiness value= " + b)
    allHappinessValue.filter(_ == b).size
  }

  var t = 1
  for(ar <- allArgs) {
    var output = getHappiness(ar._1,ar._2,ar._3,ar._4)
    println("For test case " + t + " Output is: " + output +"\n")
    t+=1
  }
}
}


