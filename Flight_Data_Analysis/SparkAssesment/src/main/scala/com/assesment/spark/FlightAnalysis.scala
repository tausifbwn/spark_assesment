package com.assesment.spark

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql._
import org.apache.spark.sql.functions.{substring, _}
import org.apache.spark.sql.types._
object FlightAnalysis extends  App {
  override def main(args: Array[String]): Unit = {

    if (args.length != 2) {
      println("invalid number of arguments...")
      System.exit(1)
    }
    val inputCsv = args(0)
    val output_folder = args(1)


    val spark = SparkSession.builder.enableHiveSupport.appName("Flight Data Analysis").master("local")
      .config("COMPRESSION_CODEC", "snappy")
      .config("spark.sql.sources.partitionOverwriteMode", "dynamic")
      .config("spark.sql.csv.parser.columnPruning.enabled","false")
      .getOrCreate()
    val sc = spark.sparkContext
    import spark.implicits._

    val flightSchema = StructType(Array(
      StructField("Year", StringType, false),
      StructField("Month", StringType, false),
      StructField("DayofMonth", StringType, false),
      StructField("DayOfWeek", StringType, false),
      StructField("DepTime", StringType, true),
      StructField("CRSDepTime", StringType, true),
      StructField("ArrTime", StringType, true),
      StructField("CRSArrTime", StringType, true),
      StructField("UniqueCarrier", StringType, true),
      StructField("FlightNum", StringType, false),
      StructField("TailNum", StringType, false),
      StructField("ActualElapsedTime", StringType, true),
      StructField("CRSElapsedTime", StringType, true),
      StructField("AirTime", StringType, true),
      StructField("ArrDelay", StringType, true),
      StructField("DepDelay", StringType, true),
      StructField("Origin", StringType, true),
      StructField("Dest", StringType, true),
      StructField("Distance", StringType, true),
      StructField("TaxiIn", StringType, true),
      StructField("TaxiOut", StringType, true),
      StructField("Cancelled", StringType, true),
      StructField("CancellationCode", StringType, true),
      StructField("Diverted", StringType, true),
      StructField("CarrierDelay", StringType, true),
      StructField("WeatherDelay", StringType, true),
      StructField("NASDelay", StringType, true),
      StructField("SecurityDelay", StringType, true),
      StructField("LateAircraftDelay", StringType, true) ))

    val colNames = flightSchema.fieldNames
    //Year,Month,DayofMonth,DayOfWeek,DepTime,CRSDepTime,ArrTime,CRSArrTime,UniqueCarrier,FlightNum,TailNum,
    //ActualElapsedTime,CRSElapsedTime,AirTime,ArrDelay,DepDelay,Origin,Dest,Distance,TaxiIn,TaxiOut,Cancelled,
    // CancellationCode,Diverted,CarrierDelay,WeatherDelay,NASDelay,SecurityDelay,LateAircraftDelay;
    //val flightRdd = sc.textFile("C:/Users/mfidrows/Desktop/MyProjects/dataset_flight_raw.csv")
    //var flightDF = spark.read
    //                    .format("com.databricks.spark.csv")
    //                    .option("header", "false")
    //                    .option("mode", "DROPMALFORMED")
    //                    .schema(flightSchema)
    //                    .load("C:/Users/mfidrows/Desktop/MyProjects/dataset_flight_raw1.csv")

    //val inputRDD = sc.textFile("C:/Users/mfidrows/Desktop/MyProjects/dataset_flight_raw1.csv")
    val inputRDD = sc.textFile(s"$inputCsv")
    val filteredRDD = inputRDD.mapPartitionsWithIndex((i,itr) => (if (i==0) itr.toList.drop(8).iterator else itr ) )

    def getRow(l: List[String]): Row = {Row(l(0), l(1), l(2), l(3), l(4), l(5), l(6), l(7), l(8), l(9), l(10), l(11),
                                            l(12), l(13), l(14), l(15), l(16), l(17), l(18), l(19), l(20), l(21), l(22),
                                            l(23), l(24), l(25), l(26), l(27), l(28)) }
    val flightDF = spark.createDataFrame( filteredRDD.map(r => r.split(',').toList).map(x=>getRow(x)) , flightSchema)
    //val flightDF = filteredRDD.toDF.rdd.map(r => r(0).sp) //(colNames.mkString("\"", "\",\"" , "\""))
    //flightDF.show
    val flightDF1 = flightDF.withColumn("LateAircraftDelay",regexp_replace($"LateAircraftDelay", ";", ""))
      .na.replace(colNames , Map("NA" -> "")).na.fill("")
      .withColumn("Year",lpad($"Year", 4, "0"))
      .withColumn("Month",lpad($"Month", 2, "0"))
      .withColumn("DayofMonth",lpad($"DayofMonth", 2, "0"))
      .withColumn("DepTime",lpad($"DepTime", 4, "0"))
      .withColumn("DepTime", to_timestamp(concat_ws("-", $"Year", $"Month", $"DayofMonth", substring($"DepTime",-4, 2), substring($"DepTime",-2, 2)),"yyyy-MM-dd-HH-mm"))
      .withColumn("CRSDepTime",lpad($"CRSDepTime", 4, "0"))
      .withColumn("CRSDepTime", to_timestamp(concat_ws("-", $"Year", $"Month", $"DayofMonth", substring($"CRSDepTime",-4, 2), substring($"CRSDepTime",-2, 2)),"yyyy-MM-dd-HH-mm"))
      .withColumn("ArrTime",lpad($"ArrTime", 4, "0"))
      .withColumn("ArrTime", to_timestamp(concat_ws("-", $"Year", $"Month", $"DayofMonth", substring($"ArrTime",-4, 2), substring($"ArrTime",-2, 2)),"yyyy-MM-dd-HH-mm"))
      .withColumn("CRSArrTime",lpad($"CRSArrTime", 4, "0"))
      .withColumn("CRSArrTime", to_timestamp(concat_ws("-", $"Year", $"Month", $"DayofMonth", substring($"CRSArrTime",-4, 2), substring($"CRSArrTime",-2, 2)),"yyyy-MM-dd-HH-mm"))
      .withColumn("ActualElapsedTime", $"ActualElapsedTime".cast(IntegerType))
      .withColumn("CRSElapsedTime", $"CRSElapsedTime".cast(IntegerType))
      .withColumn("AirTime", $"AirTime".cast(IntegerType))
      .withColumn("ArrDelay", $"ArrDelay".cast(IntegerType))
      .withColumn("DepDelay", $"DepDelay".cast(IntegerType))
      .withColumn("Distance", $"Distance".cast(IntegerType))
      .withColumn("TaxiIn", $"TaxiIn".cast(IntegerType))
      .withColumn("TaxiOut", $"TaxiOut".cast(IntegerType))
      .withColumn("Cancelled", $"Cancelled".cast(IntegerType))
      .withColumn("CarrierDelay", $"CarrierDelay".cast(IntegerType))
      .withColumn("WeatherDelay", $"WeatherDelay".cast(IntegerType))
      .withColumn("NASDelay", $"NASDelay".cast(IntegerType))
      .withColumn("SecurityDelay", $"SecurityDelay".cast(IntegerType))

    flightDF1.cache

    var arrDelayDF = flightDF1.filter($"ArrDelay" > 20)
                      .select(
                      count("*").as("Over20minDelayOccurance"),
                      (sum(when($"CarrierDelay" > 0 , 1).otherwise(0))*100/count("*")).alias("CarrierDelayOccurancePct"),
                      (sum($"CarrierDelay") / sum(when($"CarrierDelay" > 0 , 1).otherwise(0))).as("AvgCarrierDelay"),
                      (sum(when($"WeatherDelay" > 0 , 1).otherwise(0))*100/count("*")).alias("WeatherDelayOccurancePct"),
                      (sum($"WeatherDelay") / sum(when($"WeatherDelay" > 0 , 1).otherwise(0))).as("AvgWeatherDelay"),
                      (sum(when($"NASDelay" > 0 , 1).otherwise(0))*100/count("*")).alias("NASDelayOccurancePct"),
                      (sum($"NASDelay") / sum(when($"NASDelay" > 0 , 1).otherwise(0))).as("AvgNASDelay"),
                      (sum(when($"SecurityDelay" > 0 , 1).otherwise(0))*100/count("*")).alias("SecurityDelayOccurancePct"),
                      (sum($"SecurityDelay") / sum(when($"SecurityDelay" > 0 , 1).otherwise(0))).as("AvgSecurityDelay"),
                      (sum(when($"LateAircraftDelay" > 0 , 1).otherwise(0))*100/count("*")).alias("LateAircraftDelayOccurancePct"),
                      (sum($"LateAircraftDelay") / sum(when($"LateAircraftDelay" > 0 , 1).otherwise(0))).as("AvgLateAircraftDelay") )

    /*
    //Writing the flight data to Hive table in insert-update fashion
    //Considering Keys are Year,Month,DayofMonth,CRSDepTime,FlightNum and TailNum
    val tableDF = spark.sql("select * from dbprod.flight_details")
    val oldDF = tableDF.join(flightDF1,Seq("Year","Month","DayofMonth","CRSDepTime","FlightNum","TailNum"),"leftanti")
    val newDF = flightDF1.join(tableDF,Seq("Year","Month","DayofMonth","CRSDepTime","FlightNum","TailNum"),"leftanti")
    val updateDF = flightDF1.join(tableDF,Seq("Year","Month","DayofMonth","CRSDepTime","FlightNum","TailNum"),"leftsemi")

    val insertDF = oldDF.union(newDF).union(updateDF)

    insertDF.write.mode(SaveMode.Overwrite).insertInto("dbprod.flight_details")
     */


    var delayedOrCancelledDF = flightDF1.select(
                      sum(when($"ArrDelay" > 0 , 1).otherwise(0)).alias("ArrDelayCount"),
                      sum($"Cancelled").alias("CancelledCount"))

    flightDF1.write.parquet(s"$output_folder/enriched_dataset_parquet")

    flightDF1.coalesce(1).write.format("com.databricks.spark.csv").mode(SaveMode.Overwrite).option("header","true").option("nullValue", "").save(s"$output_folder/enriched_dataset_csv")

    arrDelayDF.coalesce(1).write.format("com.databricks.spark.csv").mode(SaveMode.Overwrite).option("header","true").option("nullValue", "").save(s"$output_folder/Delay_Analysis")

    delayedOrCancelledDF.coalesce(1).write.format("com.databricks.spark.csv").mode(SaveMode.Overwrite).option("header","true").option("nullValue", "").save(s"$output_folder/Delay_Or_Cancelled_count")

    flightDF1.unpersist

  }  //End of Main

}  //End of Object
