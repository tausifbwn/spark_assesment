This Project is used to Analysis Flight Data

The JAR present in SparkAssesment/target is used run in in the cluster

It can be triggered using Scripts/flight_data_analysis.sh

Sample Input data is present as Input/dataset_flight_raw.csv

Output files are present in Output folder

Sample test evidences are present in Testing Folder

Note: This project is built using IntelliJ with Scala 2.11.12 and to be run on Spark 2.4.0


