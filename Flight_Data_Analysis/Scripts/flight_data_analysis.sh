#!/bin/bash

##############################################################################################################
## This script will be used to insert source csv file for flight details into hive table.
## and aslo write it into a parquet file
## Revision History
##-------------------------------------------------------
## 07/09/2021	  Md Tausif     Script created
##
###############################################################################################################

echo "Starting the Flight Analysis job"


#submitting the spark job in cluster
spark2-submit \
--class com.assesment.spark.FlightAnalysis \
--master yarn --deploy-mode client \
--executor-memory 2G --num-executors 2 --executor-cores 2 --driver-memory 2G \
--conf "spark.dynamicAllocation.enabled=false" \
--name SparkAssesment \
/home/mfidrows/SparkAssesment.jar "/tmp/tausif/dataset_flight_raw.csv" "/tmp/tausif/output" > /home/mfidrows/logs/SparkAssesment.log 2>&1


RC=$?
if [ $RC != "0" ]; then
	echo "ERROR: Error while running spark process for flight details"
	exit 1
fi;

echo "INFO: Script completed"
